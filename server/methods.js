import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';

import { Meteor } from 'meteor/meteor';
import { Tasks } from '../imports/api/tasks.js';
import { Images } from '../imports/api/images.js';

Meteor.methods({
  insertUser: (user, submitter) => {
    if (Roles.userIsInRole(submitter, 'manager')){
      return Accounts.createUser(user);
    } else {
      throw new Meteor.Error(`You're not authorized to perform this action`);
    }
  },
  updateUserFields: function(id, fields){
    Meteor.users.update({_id: id}, {$set: fields});
  },
  updateAvatar: function(id, avatarData){
    Meteor.users.update({_id: id}, { $set: { avatar: avatarData }});
  },
  updateUserTasks: function(id, data){
    Meteor.users.update({_id: id}, {$set: {tasks: data}});
  },
  updateUserData: (id, data) => Meteor.users.update({_id: id}, {$set: data}),
  updateUserPassword: (id, password) => {
    Accounts.setPassword(id, password);
  },
  removeImage: (id) => Images.remove({ _id: id}),
  assignRole: (id, role) => Roles.setUserRoles(id, role),
});
