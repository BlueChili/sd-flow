import { Meteor } from 'meteor/meteor';
// import { Accounts } from 'meteor/accounts-base';
import { Orders } from '../imports/api/orders.js';
import { Clients } from '../imports/api/clients.js';
import { Tasks } from '../imports/api/tasks.js';
// import { Images } from '../imports/api/images.js';

Meteor.publish(`orders.Dashboard`, () => Orders.find({complete: false}));

Meteor.publish(`orders.availableOrders`, function(){
  //need to redefine the cursor according to the completed property
  const orders = Orders.find({});
  return orders;
});

Meteor.publish(`clients.addOrderList`,() => Clients.find({}, {fields: {name: 1}}));

Meteor.publish(`clients.management`, () => Clients.find({}));

Meteor.publish(`clients.Dashboard`, () => Clients.find({}, {fields: { name: 1 }}));

Meteor.publish(`clients.fullClients`, () => Clients.find({}));

Meteor.publish(`clients.viewOrderClient`, (id) => Clients.find({_id: id}));

Meteor.publish(`clients.getCreatedClient`, function(id){
  if (id) return Clients.find({_id: id}, {name:true});
  return false;
});

Meteor.publish(`orders.editOrder`, (orderId) => Orders.find({_id: orderId}));

Meteor.publish(`orders.archivedOrders`, () => Orders.find({complete: true}));

Meteor.publish(`orders.clientOrders`, (clientId) => Orders.find({customer: clientId}));

Meteor.publish(`mainUsersList`, function(){
  return Meteor.users.find({},{
    fields: {
      avatar: 1,
      lastName: 1,
      firstName: 1,
      emails: 1,
      tasks: 1,
    }
  });
});

Meteor.publish(`userInfo`, function(id){
  const projection = {
    avatar: 1,
    lastName: 1,
    firstName: 1,
    roles: 1,
    emails: 1,
  };
  return Meteor.users.find({_id: id},{ fields: projection});
});

Meteor.publish(`users.currentTasks`, (id) => Meteor.users.find({_id: id}, {fields: {tasks:1}}));

Meteor.publish(`reports`, (filter) => filter ? Orders.find(filter) : Orders.find({complete: false}));
