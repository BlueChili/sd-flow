import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Orders } from '../imports/api/orders.js';
import { Clients } from '../imports/api/clients.js';
import { Roles } from 'meteor/alanning:roles';
// import { Images } from '../imports/api/images.js';

import './publications.js';
import './methods.js';
// import '../imports/config/at_config.js';

const seedData = [
  {
    invoice: parseInt(Math.random() * 800),
    customer: 'fiC74hSRNjopBGEeS',
    telephone: parseInt(Math.random() * 9000000),
    email: 'johndoe@email.com',
    description: 'Booklet',
    statuses: {
      emailInfo: { label: 'Email Info', state: 'Completed', value: true },
      approval: { label: 'Approval needed', value: false , state: false },
      readyForPrint: { label: 'Ready for print', value: true , state: 'Stand By' },
      designWork: { label: 'Design', value: true, state: 'Completed' },
    },
    finishings: [
      { identifier: 'numbered', label: 'Laminating', value: false },
      { identifier: 'uvCoating', label: 'Outsourcing', value: false },
      { identifier: 'raising', label: 'Die cutting', value: false },
      { identifier: 'grommet', label: 'Cutting', value: false },
      { identifier: 'saddleStitch', label: 'Perfect binding', value: false },
      { identifier: 'perforated', label: 'Embroidery', value: false },
      { identifier: 'binding', label: 'Installation', value: false },
      { identifier: 'folding', label: 'Direct mailing', value: true },
      { identifier: 'minolta', label: 'Silkscreen', value: true },
      { identifier: 'ryobi', label: 'Digital', value: false },
      { identifier: 'roland', label: 'Offset', value: false },
      { identifier: 'hp', label: 'Hp', value: false},
      { identifier: 'offset', label: 'Roland', value: false},
      { identifier: 'digital', label: 'Ryobi', value: false},
      { identifier: 'silkscreen', label: 'Minolta', value: false},
      { identifier: 'directMailing', label: 'Folding', value: false},
      { identifier: 'installation', label: 'Binding', value: false},
      { identifier: 'embroidery',  label: 'Perforated', value: false},
      { identifier: 'perfectBinding', label: 'SaddleStitch', value: false},
      { identifier: 'cutting',  label: 'Grommet', value: false},
      { identifier: 'dieCutting', label: 'Realce/Raising', value: false},
      { identifier: 'outsourcing', label: 'UV coating', value: false},
      { identifier: 'laminating', label: 'Numbered', value: false},
    ],
    jobReady: 'ups',
    specs: {
      quantity: { label: 'Quantity', value: 1000 },
      description: { label: 'Item', value: 'flyer'},
      size: { label: 'Size', value: 'a4'},
      material: { label: 'Material', value: 'matte'},
    },
    comments: [],
    deposit: 1000,
    total: 2000,
    balance: 1000,
    inDate: 'Oct 12 2016',
    dueDate: 'Nov 01 2016',
    priority: 'high',
    designatedWorkers: ['Caty'],
  },
  {
    invoice: parseInt(Math.random() * 800),
    customer: 'fiC74hSRNjopBGEeS',
    telephone: parseInt(Math.random() * 9000000),
    email: 'jeannedoe@email.com',
    description: 'Invitation letters',
    statuses: {
      emailInfo: { label: 'Email Info', value: true, state: 'Completed'},
      approval: { label: 'Approval needed', value: true, state: 'Stand By' },
      readyForPrint: { label: 'Ready for print', value: true, state: 'Stand By' },
      designWork: { label: 'Design', value: true, state: 'Completed' },
    },
    finishings: [
      { identifier: 'numbered',label: 'Numbered', value: true}, 
      { identifier: 'uvCoating',label: 'UV coating', value: true}, 
      { identifier: 'raising',label: 'Realce/Raising', value: false}, 
      { identifier: 'grommet',label: 'Grommet', value: true}, 
      { identifier: 'saddleStitch',label: 'Saddle Stitch', value: false}, 
      { identifier: 'binding',label: 'Perforated', value: false}, 
      { identifier: 'perforated',label: 'Binding', value: false}, 
      { identifier: 'folding',label: 'Folding', value: false}, 
      { identifier: 'minolta',label: 'Minolta', value: false}, 
      { identifier: 'ryobi',label: 'Ryobi', value: false}, 
      { identifier: 'roland',label: 'Roland', value: false}, 
      { identifier: 'hp',label: 'Hp', value: false}, 
      { identifier: 'offset',label: 'Offset', value: false },
      { identifier: 'digital',label: 'Digital', value: false },
      { identifier: 'silkscreen',label: 'Silkscreen', value: true },
      { identifier: 'directMailing',label: 'Direct mailing', value: true },
      { identifier: 'installation',label: 'Installation', value: false },
      { identifier: 'embroidery', label: 'Embroidery', value: false },
      { identifier: 'perfectBinding',label: 'Perfect binding', value: false },
      { identifier: 'cutting', label: 'Cutting', value: false },
      { identifier: 'dieCutting',label: 'Die cutting', value: false },
      { identifier: 'outsourcing',label: 'Outsourcing', value: false },
      { identifier: 'laminating',label: 'Laminating', value: false },
    ],
    jobReady: 'fedex',
    specs: {
      quantity: {label: 'Quantity', value: 500},
      description: {label: 'Item', value: 'poster'},
      size: {label: 'Size', value: 'b3'},
      material: {label: 'Material', value: 'propalcote'},
    },
    deposit: 500,
    total: 1000,
    balance: 500,
    inDate: 'Oct 20 2016',
    dueDate: 'Nov 10 2016',
    priority: 'med',
    designatedWorkers: ['Salvador'],
    comments: [],
  },
  {
    _id: "fiC74hSRNjoPBGEeS",
    invoice: parseInt(Math.random() * 800),
    customer: 'fiC74hSRNjopBGEeS',
    telephone: parseInt(Math.random() * 9000000),
    email: 'stuart-brown@email.com',
    description: 'Book',
    statuses: {
      emailInfo: { label: 'Email Info', state: 'Completed', value: true },
      approval: { label: 'Approval needed', state: 'In progress', value: true },
      readyForPrint: { label: 'Ready for print', state: 'Stand By', value: false },
      designWork: { label: 'Design', state: 'In progress', value: true },
    },
    finishings: [
      { identifier: 'numbered', label: 'Numbered', value: false},
      { identifier: 'uvCoating', label: 'UV coating', value: false},
      { identifier: 'raising', label: 'Realce/Raising', value: true},
      { identifier: 'grommet', label: 'Grommet', value: false},
      { identifier: 'saddleStitch', label: 'SaddleStitch', value: false},
      { identifier: 'perforated', label: 'Perforated', value: true},
      { identifier: 'binding', label: 'Binding', value: true},
      { identifier: 'folding', label: 'Folding', value: true},
      { identifier: 'minolta', label: 'Minolta', value: false},
      { identifier: 'ryobi', label: 'Ryobi', value: false},
      { identifier: 'roland', label: 'Roland', value: false},
      { identifier: 'hp', label: 'Hp', value: false},
      { identifier: 'offset', label: 'Offset', value: false },
      { identifier: 'digital', label: 'Digital', value: false },
      { identifier: 'silkscreen', label: 'Silkscreen', value: true },
      { identifier: 'directMailing', label: 'Direct mailing', value: true },
      { identifier: 'installation', label: 'Installation', value: false },
      { identifier: 'embroidery', label: 'Embroidery', value: false },
      { identifier: 'perfectBinding', label: 'Perfect binding', value: false },
      { identifier: 'cutting', label: 'Cutting', value: false },
      { identifier: 'dieCutting', label: 'Die cutting', value: false },
      { identifier: 'outsourcing', label: 'Outsourcing', value: false },
      { identifier: 'laminating', label: 'Laminating', value: false },
    ],
    jobReady: 'taxi',
    specs: {
      quantity: {label: 'Quantity', value: 2000, fieldName: 'specsQuantity', fieldType: 'number'},
      size: {label: 'Size', value: 'half letter', fieldName: 'specsSize', fieldType: 'text'},
      description: {label: 'Item Description', value: 'booklet', fieldName: 'specsDescription', fieldType: 'text'},
      material: {label: 'Material', value: '100g', fieldName: 'specsMaterial', fieldType: 'text'},
    },
    deposit: 1500,
    total: 3000,
    balance: 1500,
    inDate: 'Oct 24 2016',
    dueDate: 'Nov 06 2016',
    priority: 'high',
    designatedWorkers: ['Salvador'],
    comments: [],
  },
];

Meteor.startup(() => {
  Orders._ensureIndex({ "complete": 1});
  Orders._ensureIndex({ "customer": 1});
  // code to run on server at startup
  // Clients.remove({});
  // Clients.insert({
  //   _id: 'fiC74hSRNjopBGEeS',
  //   name: 'example client',
  //   phone: '555-666-1234',
  //   personInCharge: 'John Doe Jr.',
  //   website: 'http://example.org',
  //   address: '123 E Fake St',
  //   orders: [],
  // }, function(err, success){
  //   if (success) {
  //     Orders.remove({});
  //     seedData.forEach(doc => Orders.insert(doc));
  //   }
  // });
});

Accounts.onCreateUser((options, user) => {
  if (!Meteor.users.findOne()) {
    Roles.setUserRoles(user._id, 'manager');
    user.roles = ['manager'];
  }
  return user;
});
