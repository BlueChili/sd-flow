import { AccountsTemplates } from 'meteor/useraccounts:core';
import { Meteor } from 'meteor/meteor';

AccountsTemplates.configure({
  hideSignUpLink: Meteor.settings.public.DisableUserRegister
});

