import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { moment } from 'meteor/momentjs:moment';

import { Orders } from '../api/orders.js';

import './orders.html';

FlowRouter.route('/orderArchive', {
  name: 'orders',
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'ordersArchive'});
  },
});

Template.ordersArchive.onCreated(function(){
  this.autorun( () => {
    this.subscribe('orders.archivedOrders');
  });
});

Template.ordersArchive.helpers({
  'archivedOrders' () {
    return Orders.find({});
  },
  'date': (date) => moment(date).format("MMMM Do YYYY, h:mm a"),
});
