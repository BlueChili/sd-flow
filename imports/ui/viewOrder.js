import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';

import './viewOrder.html';
import './vo-item.html';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';

FlowRouter.route('/viewOrder/:id/:customer', {
  action: () => {
      BlazeLayout.render('mainLayout', {content: 'viewOrder'});
  },
});

Template.viewOrder.onCreated(function() {
  this.orderId = new ReactiveVar(FlowRouter.getParam('id'));
  this.customerId = new ReactiveVar(FlowRouter.getParam('customer'));
  this.autorun(() => {
    this.subscribe(`orders.editOrder`, this.orderId.get(), () => {
      this.order = Orders.findOne({_id: this.orderId.get()});
    });
    this.subscribe(`clients.viewOrderClient`, this.customerId.get(), () => {
      this.client = Clients.findOne({_id: this.customerId.get()});
    });
    this.subscribe(`mainUsersList`);
  });
});

Template.viewOrder.onDestroyed(function(){
  delete this.order;
});

Template.viewOrder.helpers({
  'customerName': () => `${Template.instance().client.name}`,
  'paymentStatus': () => {
    const order = Template.instance().order;
    if (order.paymentStatus) return `Paid`;
    return `Pending`;
  },
  'order': () => {
    return Orders.findOne({_id: Template.instance().orderId.get()}, {
      transform: (doc) => {
        if (doc.comments && doc.comments.length) {
          doc.comments.reverse();
          return doc;
        }
        return doc;
      }
    });
  },
  'moment': (ref) => moment(ref).calendar(),
});

Template.viewOrder.events({
  'click .js-post-comment' (e,t) {
    const comment = {
      author: Meteor.userId(),
      createdAt: Date.now(),
      body: t.$(`[name='comment']`).val(),
    };
    if (!comment.body.length) return false;
    Orders.update(t.orderId.get(), {$push: { comments: comment }}, (err, success) => {
      if (err) return window.alert('Server Failure: comment not inserted please try again');
      if (success) return t.$(`[name='comment']`).val('');
    });
  },
});
