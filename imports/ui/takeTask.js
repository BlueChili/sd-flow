import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { _ } from 'meteor/underscore';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';

import './takeTask.html';

FlowRouter.route('/tasks', {
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'takeTask'});
  }
});

Template.takeTask.onCreated(function(){
  this.autorun(() => {
    this.subscribe('orders.availableOrders');
    this.subscribe('users.currentTasks', Meteor.userId());
    this.subscribe('clients.Dashboard');
  });
});

Template.takeTask.helpers({
  availableOrders: () => Orders.find({}),
  clientName: (id) => Clients.findOne({_id: id}).name,

  orderListChecker (id){
    const userTasks = Meteor.users.findOne(Meteor.userId()).tasks;
    return _.some(userTasks, (task) => id == task.order);
  }
});

Template.takeTask.events({
  'click .js-updateUserTasks' (e, t){
    const userTasks = Meteor.users.findOne(Meteor.userId()).tasks;
    let currentTasks = [];
    _.each(t.findAll(`[data-identifier='orders']`), (el) => {
      if (el.checked) {
      const date = new Date();
      const task = {
        user      :Meteor.userId(),
        order     :el.getAttribute(`name`),
        start     :date.getTime(),
        year      :date.getFullYear(),
        month     :date.getMonth(),
        active    :true,
        };
        /*
          now i have to check if there is a task with the same orderid
          if so retrieve the task and push it to a store
         */
        if (_.some(userTasks, (x) => x.order == task.order)) {
          return currentTasks.push(_.find(userTasks, (e) => e.order === task.order));
        } else {
          return currentTasks.push(task);
        }
      } else {
        /*
          Manage tasks completed an pushing them to the tasks collection
        */
        const orderId = el.getAttribute(`name`);
        if (!_.some(userTasks, (x) => x.order == orderId)) return;
        let task = _.find(userTasks, (x) => x.order == orderId);
        const order = Orders.findOne({_id: orderId});
        task[`period`] = order.tasks && order.tasks[Meteor.userId()] ? 
          order.tasks[Meteor.userId()].period + Date.now() - task.start :
          Date.now() - task.start;
        return Orders.update({_id: task.order}, {$set: {tasks: { [Meteor.userId()]: task}}});
      }
    });
    Meteor.call(`updateUserTasks`,Meteor.userId(),currentTasks);
  },
});
