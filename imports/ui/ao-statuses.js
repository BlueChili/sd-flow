import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './ao-statuses.html';

Template.aoStatuses.onCreated(function(){
  this.emailInfo = new ReactiveVar(false);
  this.readyForPrint = new ReactiveVar(false);
  this.designWork = new ReactiveVar(false);
  this.approval = new ReactiveVar(false);
});

Template.aoStatuses.events({
  'click #emailInfo' () {
    Template.instance().emailInfo.set(!Template.instance().emailInfo.get());
  },
  'click #readyForPrint' () {
    Template.instance().readyForPrint.set(!Template.instance().readyForPrint.get());
  },
  'click #designWork' () {
    Template.instance().designWork.set(!Template.instance().designWork.get());
  },
  'click #approval' () {
    Template.instance().approval.set(!Template.instance().approval.get());
  },
});

Template.aoStatuses.helpers({
  emailInfo () {
    return Template.instance().emailInfo.get();
  },
  readyForPrint () {
    return Template.instance().readyForPrint.get();
  },
  designWork () {
    return Template.instance().designWork.get();
  },
  approval () {
    return Template.instance().approval.get();
  },
});
