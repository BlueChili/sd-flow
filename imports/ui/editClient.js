import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import './editClient.html';

import { Clients } from '../api/clients.js';

FlowRouter.route('/editClient/:id', {
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'editClient'});
  },
});

Template.editClient.helpers({
  client () {
    const clientId = FlowRouter.getParam('id');
    return Clients.findOne({_id: clientId});
  },
});
