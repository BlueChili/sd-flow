import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Session } from 'meteor/session';

import { Clients } from '../api/clients.js';
import { Orders } from '../api/orders.js';

import './clients.html';

FlowRouter.route('/clients', {
  name: 'clients',
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'clients'});
  },
});

Template.clients.onCreated(function(){
  this.selectedClient = new ReactiveVar('');
  this.autorun( () => {
    this.subscribe(`clients.fullClients`);
    this.clientsList = Clients.find({});
  });
});

Template.clients.onRendered(function(){
  this.find(`[name='client-list']`).value=false;
});

Template.clients.onDestroyed(function(){
  Session.set('clientId', '');
});

Template.clients.helpers({
  clientsList () {
    return Template.instance().clientsList;
  },
  clientData: function () {
    return Clients.findOne({_id: Template.instance().selectedClient.get()});
  },
});

Template.clients.events({
  'change [name="client-list"]' (e, t) {
    t.selectedClient.set(e.target.value);
    Session.set('clientId', e.target.value);
  },
});

Template.clientToEdit.onCreated(function(){
  this.autorun(() => {
    this.subscribe(`orders.clientOrders`, Session.get('clientId'));
  });
});

Template.clientToEdit.helpers({
  'orderInvoice': (id) => Orders.findOne({_id: id}).invoice,
  'orderDescription': (id) => Orders.findOne({_id: id}).specs.description.value,
  'orderQuantity': (id) => Orders.findOne({_id: id}).specs.quantity.value,
  'orderDate': (id) => Orders.findOne({_id: id}).inDate,
});

Template.clientToEdit.events({
  'click .js-updateClient' (e, t){
    let clientProfile = {};
    _.each(t.findAll('input'), function(el){
      clientProfile[el.getAttribute(`data-identifier`)] = el.value;
    });
    Clients.update({_id: t.data._id}, {$set: clientProfile});
  },
});
