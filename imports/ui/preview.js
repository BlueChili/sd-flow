import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Blaze } from 'meteor/blaze';
import { Random } from 'meteor/random';

import './preview.html';

import { Items } from '../api/items.js';

Template.previewUploader.onCreated(function(){
  this.hasPreview = new ReactiveVar(false);
  if (this.data && this.data.fileUrl) this.hasPreview.set(true);
});

Template.previewUploader.helpers({
  'showRemoveButton': () => Template.instance().hasPreview.get() ? '' : 'hide invisible',
  'showUploadButton': () => Template.instance().hasPreview.get() ? 'hide invisible' : '',
  'getPreviewId': () => Template.instance().data._id || Random.id(),
});

Template.previewUploader.events({
  'change .previewUploaderInput' (e, t){
    let reader = new FileReader();

    reader.onload = function(e) {
      const imgNode = t.find('img');
      imgNode.src = e.target.result ;
      imgNode.classList = 'itemPreview';
      if (!t.hasPreview.get()) {
        t.hasPreview.set(!t.hasPreview.get());
        Blaze.renderWithData( Template.previewUploader
                    , { '_id': Random.id() }
                    , t.firstNode.parentElement);
      }
    }

    reader.readAsDataURL(t.find('.previewUploaderInput').files[0]);
  },

  'click .removePreview' (e, t){
    e.preventDefault();
    if (t.data.fileUrl){
      const rootItemId = Blaze.getView(t.view.parentView._domrange.parentElement).templateInstance().data._id;
      const previewToRemove = t.data._id;
      const rootNode = document.getElementById(rootItemId);
      // Items.update({_id: rootItemId}, {"$pull": { preview: { _id: previewToRemove } } });
      $(`#${previewToRemove}`).remove();
      if (rootNode.querySelectorAll('.previewUploader').length < 1) {
          Blaze.render( Template.previewUploader
                      , rootNode.querySelector('.previewsContainer'));
      }
    } else if ( $('.previewUploader').length > 1) {
      Blaze.remove( t.view );
    } else {
      Blaze.remove( t.view );
      Blaze.render( Template.previewUploader
                  , document.querySelector('.previewsContainer'));
    }
  }
});
