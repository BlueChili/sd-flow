import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Session } from 'meteor/session';

import './attachments.html';

import { Orders } from '../api/orders.js';
import { Images } from '../api/images.js';

Template.attachments.onCreated(function(){
  this.tempStorage = new ReactiveVar(FlowRouter.getRouteName() === 'addOrder');
  if (this.tempStorage.get()) {
    Session.set('attachments', []);
  } else {
    this.attachments = (id) => Orders.findOne({_id: id}).attachments;
  }
});

Template.attachments.onDestroyed(function(){
  if (this.tempStorage) {
    _.each(Session.get('attachments'), (item) => {
      Images.remove({_id: item._id});
    });
    Session.set('attachments', '');
  }
});

Template.attachments.helpers({
  'tempfiles': () => Session.get('attachments'),
  'orderAttachments': () => Orders.findOne({_id: Template.instance().data._id}).attachments,
});

Template.attachments.events({
  'change #attachment' (e,t) {
    let upload = Images.insert({
      file: t.find(`[name='files']`).files[0],
      streams: 'dynamic',
      chunkSize: 'dynamic'
    }, false);
    upload.on('uploaded', function(err, data){
      data.fileUrl = `${data._downloadRoute}/${data._collectionName}/${data._id}/original/${data._id}${data.extensionWithDot}`;
      if (t.tempStorage.get()) {
        let currentItems = Session.get('attachments');
        currentItems.push(data);
        Session.set('attachments', currentItems);
      } else {
        let currentItems = t.data.attachments ? t.data.attachments : [];
        currentItems.push(data);
        Orders.update({_id: t.data._id}, {$set: {attachments: currentItems}});
      }
    });
    upload.start();
  },
  'click .js-delete-attachment' (e, t) {
    e.preventDefault();
    Images.remove({_id: e.target.id});
    const currentAttachments = t.data.attachments ? t.data.attachments : Session.get('attachments');
    if (t.tempStorage.get()) {
      Session.set('attachments',
        _.filter(currentAttachments, (i) => i._id !== e.target.id)
      );
    } else {
      Orders.update({_id: t.data._id}, {
        $set: {
          attachments: _.filter(currentAttachments, (i) => i._id !== e.target.id)
        }
      });
    }
  },
});
