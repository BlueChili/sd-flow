import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { _ } from 'meteor/underscore';
import { ReactiveVar } from 'meteor/reactive-var';
import { moment } from 'meteor/momentjs:moment';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';

import './tasksReport.html';

FlowRouter.route('/reports', {
  action: () => BlazeLayout.render('mainLayout', {content: 'tasksReport'}),
});

Template.tasksReport.onCreated(function(){
  this.tasksFilterType = new ReactiveVar('active');
  this.tasksUserId = new ReactiveVar('');
  this.tasksFilterYear = new ReactiveVar(false);
  this.tasksFilterMonth = new ReactiveVar(false);
  this.tasksPresetFilter = new ReactiveVar('month');
  this.displayFilterType = new ReactiveVar(false);
  this.displayCustomDateFilters = new ReactiveVar(false);
  let date = new Date();
  let year = this.tasksFilterYear.get();
  let month = this.tasksFilterMonth.get();
  const filterGen = () => {
    switch (this.tasksPresetFilter.get()) {
      case 'week':
        return {
          year: date.getFullYear(), 
          month: date.getMonth(), 
          day: date.getDate() > 6 ? {$gte: date.getDate() - 6} : {$gt: 0}
        };
      case 'month':
        return {
          year: date.getFullYear(), 
          month: date.getMonth(), 
        };
      default:
        return { year: year, month: month};
    }
  };
  this.autorun(() => {
    if (this.tasksFilterType.get() == 'active') {
      this.subscribe('reports', { complete: false });
    } 
    else if (this.tasksFilterType.get() == 'user') {
      this.subscribe('reports', filterGen());
    } 
    else {
      this.subscribe('reports', filterGen());
    }
    this.subscribe('mainUsersList');
    this.subscribe('clients.Dashboard');
  });
});


Template.tasksReport.helpers({
  'tasksMerger': (x) => {
    let usersCurrentlyWorking = _.filter(Meteor.users.find({}).fetch(), (user) => {
      return _.some(user.tasks, (task) => task.order == x._id);
    });
    if (usersCurrentlyWorking[0]) {
      _.each(usersCurrentlyWorking, function (u) {
        if (_.some(x.tasks, (t) => t.user == u._id)){
          let task = _.find(u.tasks, (t) => t.order == x._id);
          x.tasks[u._id].period = x.tasks[u._id].period + (Date.now() - task.start);
        } else {
          let task =_.find(u.tasks, (t) => t.order == x._id);
          task.period = Date.now() - task.start;
          x.tasks[u._id] = task;
        }
      });
    }
    // console.log(`logging the tasks list now`, x.tasks);
    // const tasksArray = _.map(x.tasks, (i) => i);
    // let tasksArray = _.toArray(x.tasks);
    // console.log(tasksArray);
    // x.tasks = tasksArray;
    // console.log(x);
    return x;
  },
  'orders': () => Orders.find({}),
  currentUserFilterByUser: () => Template.instance().tasksUserId.get(),
  'tasksByUser': () => Template.instance().tasksFilterType.get() === 'user',
  'tasksByDate': () => Template.instance().tasksFilterType.get() === 'date',
  'tasksByActive': () => Template.instance().tasksFilterType.get() === 'active',
  'filterByDate': () => Template.instance().displayFilterType.get() == 'date',
  'filterByUser': () => Template.instance().displayFilterType.get() == 'user',
  'customDate': () => Template.instance().displayCustomDateFilters.get(),
  'years': () => {
    const currentYear = new Date();
    return _.range(2017, currentYear.getFullYear() + 1, 1);
  },
  'userList': () => Meteor.users.find({}),
});

Template.tasksReport.events({
  'change [name="filterType"]': (e, t) => {
    t.displayCustomDateFilters.set(false);
    t.displayFilterType.set(e.target.value);
  },
  'change [name="firstFilter"]' (e, t){
    if (e.target.value == 'custom') return t.displayCustomDateFilters.set(true);
    return t.displayCustomDateFilters.set(false);
  },
  'click .ApplyFilter': (e, t) => {
    const selector = t.find('[name="filterType"]');
    if (t.find(`[name='filterYear']`)) t.tasksFilterYear.set(Number(t.find('[name="filterYear"]').value));
    if (t.find(`[name='filterMonth']`)) t.tasksFilterMonth.set(Number(t.find('[name="filterMonth"]').value));
    switch (selector.value) {
      case 'user':
        t.tasksFilterType.set(selector.value);
        t.tasksUserId.set(t.find('[name="userSelector"]').value);
        if (t.find('[name="firstFilter"]').value !== 'custom') {
          t.tasksPresetFilter.set(t.find('[name="firstFilter"]').value); break;
        } 
        return t.tasksPresetFilter.set('custom');
      case 'date':
        t.tasksFilterType.set(selector.value);
        if (t.find('[name="firstFilter"]').value !== 'custom') {
          t.tasksPresetFilter.set(t.find('[name="firstFilter"]').value); break;
        }
        return t.tasksPresetFilter.set('custom');
      default:
        t.tasksFilterType.set('active');
    }
  },
});

Template.orderReport.helpers({
  'getCustomerName': (id) => {
    return Clients.findOne({_id: id}).name;
  },
  'toArray': (xs) => _.map(xs, (x) => x),
  'getUserName': (id) => {
    let user = Meteor.users.findOne({_id: id});
    return `${user.firstName} ${user.lastName}`;
  },
  timeFormatter: (period) => {
    return _.map([0,1,2], (u) => {
      switch (u) {
        case 0:
          return { 
            time: moment.duration(period).days(), 
            unit: moment.duration(period).days() == 1 ? 'Day' : 'Days'
          };
        case 1:
          return { 
            time: moment.duration(period).hours(), 
            unit: moment.duration(period).hours() == 1 ? 'Hour' : 'Hours'
          };
        default:
          return { 
            time: moment.duration(period).minutes(), 
            unit: moment.duration(period).minutes() == 1 ? 'Minute' : 'Minutes'
          };
      }
    });
  },
});
