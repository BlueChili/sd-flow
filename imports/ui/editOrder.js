import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Session } from 'meteor/session';
import { Roles } from 'meteor/alanning:roles';

import { itemTrasher, imageUploader, itemSubmitter, orderEditor } from './itemUtils.js'

import './editOrder.html';

import './attachments.js';

import './itemForm.js';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';
import { Images } from '../api/images.js';
import { Items } from '../api/items.js';

FlowRouter.route('/editOrder/:id', {
  action: () => {
      // BlazeLayout.render('mainLayout', {content: 'editOrder'});
    if (Roles.userIsInRole(Meteor.userId(), ['manager','editor'])) {
      BlazeLayout.render('mainLayout', {content: 'editOrder'});
    } else {
      window.alert(`You are not authorized to access this area
redirecting to dashboard`);
      FlowRouter.go('/');
    }
  },
});

Template.inLineComments.onCreated(function(){
  this.autorun(() => {
    this.subscribe(`mainUsersList`);
  });
});

Template.inLineComments.helpers({
  getUserName(id) {
    const user = Meteor.users.findOne({_id: id});
    return `${user.firstName} ${user.lastName}`;
  },
});

Template.editOrder.onCreated(function() {
  this.orderId = new ReactiveVar(FlowRouter.getParam('id'));
  this.draftPreview = new ReactiveVar(false);
  this.autorun(() => {
    this.subscribe(`orders.editOrder`, this.orderId.get(), {
      onReady: () => {
        this.order = Orders.findOne({_id: this.orderId.get()});
        _.each(this.order.items, (item) => Items.insert(item));
      }
    });
    this.subscribe(`clients.management`);
  });
});

Template.editOrder.onDestroyed(function(){
  delete this.order;
  Items.remove({});
});

Template.editOrder.onRendered(function() {
  this.autorun(() => {
    if (this.subscriptionsReady()){
      $(`[name='priority']`).val(this.order.priority);
      $(`[name='staff']`).val(this.order.designatedWorkers.join(', '));
      $(`#${this.order.jobReady}`).prop(`checked`, true);
      this.find(`#customerName`).value = this.order.customer;
      this.find(`[name='approvalState']`).value=this.order.statuses.approval.state;
      this.find(`[name='emailInfoState']`).value=this.order.statuses.emailInfo.state;
      this.find(`[name='readyForPrintState']`).value=this.order.statuses.readyForPrint.state;
      this.find(`[name='designWorkState']`).value=this.order.statuses.designWork.state;
    }
  });
});

Template.editOrder.helpers({
  order () {
    return Orders.findOne({ _id: Template.instance().orderId.get() }, {
      transform: (doc) => {
        if (doc.comments && doc.comments.length) {
          doc.comments.reverse();
        }
        return doc;
      }
    });
  },
  storedItems: () => Items.find({}),
  clientsList: () => Clients.find({}),
});

Template.editOrder.events({
  'click .js-make-active' (e, t){
    e.preventDefault();
    Orders.update({_id: t.order._id}, {$set: {complete: false}});
    FlowRouter.go('/dashboard');
  },
  'click .js-make-complete' (e, t){
    e.preventDefault();
    let order = formCrawler(t);
    order['complete'] = true;
    Orders.update(t.orderId.get(), {$set: order}, function(err, success){
      if (err) throw new Meteor.Error(err);
      if (success) {
        t.draftPreview.set(false);
        FlowRouter.go('/dashboard');
      }
    });
  },
  'click .js-post-comment' (e,t) {
    const comment = {
      author: Meteor.userId(),
      createdAt: Date.now(),
      body: t.find(`[name='comment']`).value
    };
    if (!comment.body.length) return false;
    Orders.update(t.orderId.get(), { "$push": { comments: comment }}, (err, success) => {
      if (err) return window.alert('Server Failure: comment not inserted please try again');
      if (success) return t.$(`[name='comment']`).val('');
    });
  },
  'click #submit' (e, t){
    e.preventDefault();
    const storedItems = t.order.items;
    const itemForms = t.findAll('.itemForm')
    const submittableItemForms = itemForms.filter(f => {
      return _.every(f.querySelectorAll('[data-group="specs"]'), x => x.value) || !f.dataset.new;
    });

    Promise.all(submittableItemForms.map(async function(x) {
      let storedItem, survivors;
      const instance = Blaze.getView(x).templateInstance();
      const targets = instance.findAll('.previewUploaderInput').filter(i => i.files.length);
      if (!instance.data.new){
        storedItem = _.findWhere(storedItems, {_id: instance.data._id});
        survivors = previewsPreflight(x, instance, storedItem);
      }
      let previews = await Promise.all(targets.map(node => imageUploader(node)));
      return await ((survivors && survivors.length) && targets.length)
                    ? itemSubmitter(instance, survivors.concat(previews))
                    : (survivors && survivors.length)
                    ? itemSubmitter(instance, survivors)
                    : (targets.length)
                    ? itemSubmitter(instance, previews)
                    : itemSubmitter(instance, false)
    }))
    .then((items) => {
      const newOrder = orderEditor(t, items);
      itemTrasher(storedItems, itemForms)
      Orders.update(t.orderId.get(), {$set: newOrder});
    })
    .catch( err => console.log( err ));
  },
});

function previewsPreflight (form, instance, item){
  if (!item.preview.length) return false;
  const sourceIds = _.pluck(item.preview, '_id');
  const DOMIds = instance.findAll('.previewUploader').map(x => x.id);
  const trashed = sourceIds.filter(x => !DOMIds.some(y => x == y));
  const survivors = sourceIds.filter(x => DOMIds.some(y => x == y));
  if (trashed.length) Images.remove({_id: {"$in": trashed} });
  return _.filter(item.preview, x => _.some(survivors, y => y === x._id));
}
