import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Blaze } from 'meteor/blaze';
import { Random } from 'meteor/random';

import { Items } from '../api/items.js';

import './itemForm.html';

import './preview.js';

Template.itemForm.onCreated(function() {
  this.storedItem = new ReactiveVar(!this.data.new);
  this.itemStatus = new ReactiveVar('');
  if (this.storedItem.get()) {
    this.itemStatus.set('primary');
  }
});

Template.itemForm.helpers({
  getId: () => Template.instance().data._id || Random.id(),
  generateId: () => Random.id(),
  showRemove: () => Template.instance().storedItem.get(),
  submittableItem: () => Template.instance().itemStatus.get(),
});

Template.itemForm.events({
  'blur [data-group="specs"]' (e, t) {
    if (t.data.new) {
      if (t.findAll('[data-group="specs"]').every(x => x.value)) {
        t.itemStatus.set('success');
      } else {
        t.itemStatus.set('');
      }
    }
  },
  'click .addItem' (e, t) {
    e.preventDefault();
    t.storedItem.set(true);
    t.find('.js-addItemRow').remove();
    Blaze.renderWithData(Template.itemForm,
                          { new: true, _id: Random.id() },
                          document.querySelector('#itemsContainer'));
  },
  'click .js-removeItem': (e, t) => {
    e.preventDefault();
    if (t.data.new){
      Blaze.remove(Template.instance().view);
    } else {
      Items.remove({_id: t.data._id});
    }
  },
});
