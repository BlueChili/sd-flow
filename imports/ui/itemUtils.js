import { Images } from '../api/images.js';

export function imageUploader (n) {

  return new Promise(function(resolve, reject){
      if (!n.files.length) resolve(false);
      Images.insert({
        file: n.files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic',
        onUploaded: function(err, data){
          data.fileUrl = `${data._downloadRoute}/${data._collectionName}/${data._id}/original/${data._id}${data.extensionWithDot}`;
          resolve(data);
        },
        onError: function(err) {
          reject(err);
        }
      });
  });

}

export function itemSubmitter (t, preview) {
  let item = {
    "_id": t.find('.itemForm').id,
    preview: preview,
    finishings: t.findAll(`[data-key='finishings']`).map((x) => {
      return { 
        value: x.checked,
        identifier: x.getAttribute('name'),
        label: x.getAttribute('data-label')
      };
    }),
  };

  _.each(t.findAll(`[data-group='specs']`), (el) => {
    item[el.getAttribute(`name`)] = {
      identifier: el.getAttribute(`name`),
      value: el.getAttribute(`name`) == 'quantity' ? Number(el.value) : el.value,
      label: el.getAttribute(`data-label`)
    };
  });

  return item;
}

export function orderEditor(t,items) {
  let order = {
    customer: t.find('#customerName').value,
    invoice: t.find('#invoice').value,
    dueDate: t.find(`[name='dueDate']`).value,
    paymentStatus: t.find(`[name='paymentStatus']`).value,
    priority: t.find(`[name='priority']`).value,
    items: items,
    statuses: {
      emailInfo: { label: 'Email Info',
      state: t.find(`[name='emailInfo']`).checked ? t.find(`[name='emailInfoState']`).value : false ,
      value: t.find(`[name='emailInfo']`).checked },
      readyForPrint: { label: 'Ready for print',
      state: t.find(`[name='readyForPrint']`).checked ? t.find(`[name='readyForPrintState']`).value : false,
      value: t.find(`[name='readyForPrint']`).checked },
      designWork: { label: 'Design',
      state: t.find(`[name='designWork']`).checked ? t.find(`[name='designWorkState']`).value : false,
      value: t.find(`[name='designWork']`).checked },
      approval: { label: 'Approval',
      state: t.find(`[name='approval']`).checked ? t.find(`[name='approvalState']`).value : false ,
      value: t.find(`[name='approval']`).checked },
    },
    jobReady: t.find('input[name="jobReady"]:checked').value,
    designatedWorkers: t.find(`[name='staff']`).value.split(', '),
  };

  if (t.order.customer != order.customer){
    let orders =  _.filter(
      Clients.findOne({_id: t.order.customer}).orders,
      (orderId) => orderId != t.order._id
    );
    Clients.update({_id: t.order.customer}, {$set: { orders: orders }});
    Clients.update({_id: order.customer}, {$push: { orders: t.order._id }});
  }
  return order;
}

export function itemTrasher(storedItems, forms) {
    const trashedItems = _.reject(storedItems, (x) => forms.some(y => y.id == x._id));
    if (trashedItems.length) {
      for (const i of trashedItems){
        if (!i.preview.length) break;
        for (const p of i.preview) {
          Images.remove({_id: p._id});
        }
      }
    }
};
