import { Clients } from '../api/clients.js';

import './addOrder.html';
import './ao-newClient.html';

Template.aoNewClient.events({
  'click .js-submitNewUser' (e) {
    e.preventDefault();
    const newClient = {
      name: $('#newCustomerName').val(),
      phone: $('#newCustomerPhone').val(),
      personInCharge: $('#newPersonCharge').val(),
      website: $('#newWebsite').val(),
      email: $('#newEmail').val(),
      address: $('#newAddress').val(),
      orders: [],
    };
    Clients.insert(newClient, function(err, result){
      if (err) throw new Meteor.Error(err);
      $('#customerName').val(result);
      $('#clientReveal').foundation('close');
      $('#newCustomerName').val('');
      $('#newCustomerPhone').val('');
      $('#newPersonCharge').val('');
      $('#newWebsite').val('');
      $('#newEmail').val('');
      $('#newAddress').val('');
    });
  },
});
