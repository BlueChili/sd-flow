import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Blaze } from 'meteor/blaze';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Mongo } from 'meteor/mongo';
import { imageUploader, itemSubmitter } from './itemUtils.js';

import './addOrder.html';

import './itemForm.js';
import './ao-statuses.js';
import './ao-newClient.js';
import './attachments.js';

import './ao-itemFinishings';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';

FlowRouter.route('/addOrder', {
  name: 'addOrder',
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'addOrder'});
  },
});

Template.addOrder.onCreated(function() {
  this.autorun(() => {
    this.subscribe(`clients.addOrderList`);
  });
});

Template.addOrder.onRendered(function(){
  this.clientReveal = new Foundation.Reveal($('#clientReveal'), {'data-overlay': false});
});

Template.addOrder.onDestroyed(function() {
  $(`#clientReveal`).remove();
  this.clientReveal.destroy();
});

Template.addOrder.events({
  'click [data-open="clientReveal"]': (e) => e.preventDefault(),
  'click #submit' (e, t){
    e.preventDefault();
    const itemForms = t.findAll('.itemForm')
    const submittableItemForms = itemForms.filter(f => {
      return _.every(f.querySelectorAll('[data-group="specs"]'), x => x.value) || !f.dataset.new;
    });

    Promise.all(submittableItemForms.map(async function(x) {
      const instance = Blaze.getView(x).templateInstance();
      const targets = instance.findAll('.previewUploaderInput').filter(i => i.files.length);
      let previews = await Promise.all(targets.map(node => imageUploader(node)));
      return await (targets.length)
                    ? itemSubmitter(instance, previews)
                    : itemSubmitter(instance, false)
    }))
    .then((items) => {
      orderSubmitter(t, items);
    })
    .catch( err => console.log( err ));
  },
});

Template.addOrder.helpers({
  'clientsList': () => Clients.find({}),
  'newItemID': () => Random.id(),
});

function orderSubmitter(t, items){
  let date = new Date();
  let order = {
    createdAt: date.getTime(),
    year: date.getFullYear(),
    month: date.getMonth(),
    day: date.getDate(),
    owner: Meteor.userId,
    customer: t.find('#customerName').value,
    invoice: t.find('#invoice').value,
    dueDate: t.find(`[name='dueDate']`).value,
    paymentStatus: t.find(`[name='paymentStatus']`).value,
    priority: t.find(`[name='priority']`).value,
    staff: t.find(`[name='staff']`).value,
    statuses: {},
    items: items,
    jobReady: t.find('input[name="jobReady"]:checked').value || '',
    designatedWorkers: t.find(`[name='staff']`).value.split(', '),
    complete: false,
    comments: t.find(`[name='comments']`).value ? [{
      author: Meteor.userId(),
      createdAt: date,
      body: t.find(`[name='comments']`).value
    }] : [],
  };
  _.each(t.findAll(`[data-statuses]`), function(el){
    order.statuses[el.getAttribute(`id`)] = {
      label: el.getAttribute(`data-label`),
      value: el.checked,
      state: el.checked 
        ? t.find(`#${el.getAttribute(`data-status-select`)}`).value
        : false,
    };
  });
  Orders.insert(order, function(err, success) {
    if (err) throw new Meteor.Error(err);
    Clients.update({_id: order.customer}, {"$push": {orders: success}});
    FlowRouter.go('/dashboard');
  });
}
