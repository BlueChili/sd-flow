import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { Roles } from 'meteor/alanning:roles';

import { Images } from '../api/images.js';

import './users.html';

FlowRouter.route('/users', {
  name: 'users',
  action: () => {
    if (Roles.userIsInRole(Meteor.userId(), 'manager')) {
      BlazeLayout.render('mainLayout', {content: 'usersRoot'});
    } else {
      window.alert(`You are not authorized to access this area
redirecting to dashboard`);
      FlowRouter.go('/');
    }
  },
});

Template.usersRoot.onCreated(function(){
  this.addUser = new ReactiveVar(false);
  this.editUser = new ReactiveVar('');
  Session.set('currentEditingUser', '');
});

Template.usersList.onCreated(function(){
  this.autorun(() => {
    this.subscribe('mainUsersList');
  });
});

Template.usersRoot.helpers({
  addingUser() {
    return Template.instance().addUser.get();
  },
  editingUser() {
    const id = Template.instance().editUser.get();
    Session.set('currentEditingUser', id);
    return id;
  },
  editedUser(id) {
    return Meteor.users.findOne({_id: id});
  },
});

Template.usersList.helpers({
  users() {
    return Meteor.users.find({});
  },
});

Template.usersRoot.events({
  'click .js-addUser' (event, instance){
    instance.editUser.set(false);
    return instance.addUser.set(true);
  },
  'click .js-user' (event, instance){
    instance.addUser.set(false);
    instance.editUser.set(this._id);
    Session.set(`passwordHelper`, false);
  },
});

Template.userEdit.onCreated(function(){
  this.userAvatar = '';
  this.autorun( () => {
    this.subscribe('userInfo', Session.get('currentEditingUser'));
  });
});

Template.userEdit.onDestroyed(function(){
  delete this.userAvatar;
  Session.set(`passwordHelper`, '');
});

Template.userEdit.helpers({
  'changePassword': () => Session.get(`passwordHelper`),
  'isOwnPassword': () => Meteor.userId() === Template.instance().data._id,
  'roleTicker': (role) => {
    return Roles.userIsInRole(Session.get(`currentEditingUser`), role) ? {selected: 'selected'} : '';
  },
});

Template.userEdit.events({
  'change .js-avatar-upload' (e, t){
    let upload = Images.insert({
      file: e.currentTarget.files[0],
      streams: 'dynamic',
      chunkSize: 'dynamic'
    }, false);
    upload.on(`error`, function (err) {
      if (err) return window.alert(`an error occurred during file upload: ${err}`);
    });
    upload.on('uploaded', function(err, data){
      if (!err) {
        data.fileUrl = `${data._downloadRoute}/${data._collectionName}/${data._id}/original/${data._id}${data.extensionWithDot}`;
        t.userAvatar = data;
        Meteor.call(`updateAvatar`, t.data._id, t.userAvatar);
      }
    });
    upload.start();
  },
  'click .js-save-user' (e,t) {
    const user = {
      firstName: t.$(`[name='firstName']`).val(),
      lastName: t.$(`[name='lastName']`).val(),
      emails: [ {
        address: t.find(`[name='email']`).value ?
          t.find(`[name='email']`).value : 
          Meteor.users.findOne({_id: Meteor.userId()}).emails[0].address,
        verified: false,
      }],
    };
    if (!Roles.userIsInRole(t.data._id, t.find(`[name='role']`).value)) {
      Meteor.call(`assignRole`, t.data._id, t.find(`[name='role']`).value);
    }
    Meteor.call('updateUserFields', t.data._id, user);
  },
  'click .js-own-password-change' (e, t) {
    e.preventDefault();
    const newP = t.find(`[name='newPassword']`);
    const reNew = t.find(`[name='reNewPassword']`);
    const old = t.find(`[name='currentPassword']`);
    if (!old.value) {
      return window.alert(`Your password is blank`);
    } else {
      if (!newP.value || !reNew.value) {
        return window.alert(`One of your new password fields is empty`);
      } else {
        if (newP.value != reNew.value) {
          return window.alert(`Your new password confirmation does not match`);
        } else {
          Accounts.changePassword(old.value, newP.value, (err) => {
            if (err) return window.alert(err);
            newP.value = '';
            reNew.value = '';
            old.value = '';
          });
        }
      }
    }
  },
  'click .js-password-change' (e, t) {
    e.preventDefault();
    const newP = t.find(`[name='newPassword']`);
    const reNew = t.find(`[name='reNewPassword']`);
    if (!newP.value || !reNew.value) {
      return window.alert(`One of your new password fields is empty`);
    } else {
      if (newP.value != reNew.value) {
        return window.alert(`Your new password confirmation does not match`);
      } else {
        if (Roles.userIsInRole(Meteor.userId(), 'manager')){
          Meteor.call(`updateUserPassword`, t.data._id, newP.value);
          newP.value = '';
          reNew.value = '';
          return window.alert(`Password succesfully changed`);
        } else {
          return window.alert(`You're not authorized to perform this action.`);
        }
      }
    }
  },
  'click .js-password' () {
    Session.set(`passwordHelper`, !Session.get(`passwordHelper`));
  },
});

Template.userAdd.events({
  'click .js-create-user' (event, t) {
    event.preventDefault();
    const user = {
      username: t.find(`[name='username']`).value,
      email : t.find(`[name='email']`).value,
      password : t.find(`[name='password']`).value,
    };
    const userData = {
      firstName : t.find(`[name='firstName']`).value,
      lastName : t.find(`[name='lastName']`).value,
    };
    Meteor.call(`insertUser`, user, Meteor.userId(), postProcess);
    function postProcess(err, id) {
      if (err) return window.alert(err);
      Meteor.call(`updateUserData`, id, userData);
      Meteor.call(`assignRole`, id, t.find(`[name='role']`).value);
    }
  },
});
