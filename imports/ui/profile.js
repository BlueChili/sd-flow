import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { _ } from 'meteor/underscore';
import { ReactiveVar } from 'meteor/reactive-var';
import { Roles } from 'meteor/alanning:roles';

import './profile.html';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';
import { Images } from '../api/images.js';

FlowRouter.route('/profile', {
  name: 'profile',
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'userProfile'});
  },
});

Template.userProfile.onCreated(function(){
  this.autorun(() => {
    this.subscribe(`userInfo`, Meteor.userId());
  });
  this.changePassword = new ReactiveVar(false);
});

Template.userProfile.helpers({
  roleTicker: (role) => 
    Roles.userIsInRole(Meteor.userId(), role) ?
    {selected: 'selected'} :
    '',
  user: () => Meteor.users.findOne({_id: Meteor.userId()}),
  changePassword: () => Template.instance().changePassword.get(),
});

Template.userProfile.events({
  'click .js-password' (e, t) {
    t.changePassword.set(!t.changePassword.get());
  },
  'click .js-own-password-change' (e, t) {
    e.preventDefault();
    const newP = t.find(`[name='newPassword']`);
    const reNew = t.find(`[name='reNewPassword']`);
    const old = t.find(`[name='currentPassword']`);
    if (!old.value) {
      return window.alert(`Your password is blank`);
    } else {
      if (!newP.value || !reNew.value) {
        return window.alert(`One of your new password fields is empty`);
      } else {
        if (newP.value != reNew.value) {
          return window.alert(`Your new password confirmation does not match`);
        } else {
          Accounts.changePassword(old.value, newP.value, (err) => {
            if (err) return window.alert(err);
            newP.value = '';
            reNew.value = '';
            old.value = '';
            window.alert(`Password successfully changed`);
          });
        }
      }
    }
  },
  'change .js-avatar-upload' (e){
    let upload = Images.insert({
      file: e.currentTarget.files[0],
      streams: 'dynamic',
      chunkSize: 'dynamic'
    }, false);
    upload.on(`error`, function (err) {
      if (err) return window.alert(`an error occurred during file upload: ${err}`);
    });
    upload.on('uploaded', function(err, data){
      if (!err) {
        data.fileUrl = `${data._downloadRoute}/${data._collectionName}/${data._id}/original/${data._id}${data.extensionWithDot}`;
        Meteor.call(`updateAvatar`, Meteor.userId(), data);
      }
    });
    upload.start();
  },
  'click .js-save-user' (e,t) {
    const userData = {
      firstName: t.find(`[name='firstName']`).value,
      lastName: t.find(`[name='lastName']`).value,
      emails: [ {
        address: t.find(`[name='email']`).value ?
          t.find(`[name='email']`).value : 
          Meteor.users.findOne({_id: Meteor.userId()}).emails[0].address,
        verified: false,
      }],
    };
    Meteor.call(`updateUserFields`, Meteor.userId(), userData, (err) => {
      if (err) {
        return window.alert(err);
      } else {
        return window.alert(`Profile updated :)`);
      }
    });
  },
});
