import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { _ } from 'meteor/underscore';

import './dashboard.html';

import { Orders } from '../api/orders.js';
import { Clients } from '../api/clients.js';
import { Images } from '../api/images.js';

FlowRouter.route('/dashboard', {
  name: 'dashboard',
  action: () => {
    BlazeLayout.render('mainLayout', {content: 'dashboard'});
  },
});

Template.dashboard.onCreated(function() {
  this.autorun( () => {
    this.subscribe('orders.Dashboard');
    this.subscribe('clients.Dashboard');
  });
  this.ordersFilter = new ReactiveVar('finishing');
});

Template.dashboard.onDestroyed(function() {
  $(`.reveal-overlay`).each(function() {$(this).remove();});
});

Template.dashboard.helpers({
  orders: () => {
    let tfilter = Template.instance().ordersFilter.get();
    let query = (tfilter === 'emailInfoFilter')
      ? {"$or": [{ "statuses.emailInfo.state": 'In progress' },
        { "statuses.emailInfo.state": 'Stand By' }] }
      : (tfilter === 'readyForPrintFilter')
      ? {"$or": [{ "statuses.readyForPrint.state": 'In progress' },
        { "statuses.readyForPrint.state": 'Stand By' }] }
      : (tfilter === 'approvalFilter')
      ? {"$or": [{ "statuses.approval.state": 'In progress' },
        { "statuses.approval.state": 'Stand By' }] }
      : (tfilter === 'designFilter')
      ? {"$or": [{ "statuses.designWork.state": 'In progress' },
        { "statuses.designWork.state": 'Stand By' }] }
      : {};

    return Orders.find(query, {
    transform: (doc) => {
      if (doc.comments && doc.comments.length) {
        doc.comments.reverse();
        return doc;
      }
      return doc;
    },
  });
  },
});

Template.dashboard.events({

  'click #filters' (e, t) {
    t.ordersFilter.set(e.target.id);
  },

});

Template.dashboardOrder.onCreated(function(){
  this.autorun(() => {
    this.subscribe(`mainUsersList`);
  });
});

Template.dashboardOrder.onRendered(function(){
  this.commentsModal = new Foundation.Reveal($(`#${this.data._id}`));
});

Template.dashboardOrder.helpers({
  totalItems: (items) => items.length,
  getClientName(id){
    return Clients.findOne({_id: id}).name;
  },
  labelColor (val) {
    switch (val) {
      case `In progress`:
        return `warning `;
      case `Completed`:
        return `success `;
      default:
        return `info`;
    }
  },
  commentCount: (comments) => comments ? comments.length : '',

  getUserName: (author) => {
    const profile = Meteor.users.findOne({_id: author});
    return `${profile.firstName} ${profile.lastName}: `;
  },
  statusArray: (statuses) => _.values(statuses),
});

Template.dashboardOrder.events({
  'click .js-delete-order' (e,t) {
    e.preventDefault();
    if (window.confirm(`Confirm you want to delete this order`)) {
      if (t.data.preview && t.data.preview._id) Images.remove({_id: t.data.preview._id});
      Orders.remove({_id: t.data._id});
    } 
  },
  'click .js-mark-complete' (e, t) {
    Orders.update({_id: t.data._id}, {$set: {complete: true }});
  },
});

Template.commentBox.events({
  'click .js-submit-comment' (e, t) {
    const comment = {
      author: Meteor.userId(),
      createdAt: new Date(),
      body: t.find(`[name='commentBox']`).value,
    };
    if (comment.body.length){
    Orders.update({_id: Template.instance().data}, {$push: {comments: comment}},
      (err, success) => {
        if (success) {
          t.find(`[name='commentBox']`).value = '';
        }
    });
    }
  },
});
