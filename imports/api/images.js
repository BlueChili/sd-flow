import { FilesCollection } from 'meteor/ostrio:files';
import { Meteor } from 'meteor/meteor';
import Grid from 'gridfs-stream';
import fs from 'fs';
import { MongoInternals } from 'meteor/mongo';

export const Images = new FilesCollection({
  debug: false,
  collectionName: 'images',
  allowClientCode: true,
  onAfterUpload(image) {
      // Move file to GridFS
      Object.keys(image.versions).forEach(versionName => {
        const metadata = { versionName, imageId: image._id, storedAt: new Date() }; // Optional
        const writeStream = gfs.createWriteStream({ filename: image.name, metadata });

        fs.createReadStream(image.versions[versionName].path).pipe(writeStream);

        writeStream.on('close', Meteor.bindEnvironment(file => {
          const property = `versions.${versionName}.meta.gridFsFileId`;

          // Convert ObjectID to String. Because Meteor (EJSON?) seems to convert it to a
          // LocalCollection.ObjectID, which GFS doesn't understand.
          this.collection.update(image._id, { $set: { [property]: file._id.toString() } });
          this.unlink(this.collection.findOne(image._id), versionName); // Unlink file by version from FS
        }));
      });
    },
  interceptDownload(http, image, versionName) {
    const _id = (image.versions[versionName].meta || {}).gridFsFileId;
    if (_id) {
      const readStream = gfs.createReadStream({ _id });
      readStream.on('error', err => { throw err; });
      readStream.pipe(http.response);
    }
    return Boolean(_id); // Serve file from either GridFS or FS if it wasn't uploaded yet
  },
  onAfterRemove(images) {
    // Remove corresponding file from GridFS
    images.forEach(image => {
      Object.keys(image.versions).forEach(versionName => {
        const _id = (image.versions[versionName].meta || {}).gridFsFileId;
        if (_id) gfs.remove({ _id }, err => { if (err) throw err; });
      });
    });
  },
});

let gfs;
if (Meteor.isServer) {
  gfs = Grid(
    MongoInternals.defaultRemoteCollectionDriver().mongo.db,
    MongoInternals.NpmModule
  );
  Images.allowClient();
}

