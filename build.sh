#!/usr/local/bin/fish

echo $argv
meteor build ./docker-build --server-only --architecture os.linux.x86_64
cd docker-build
echo 'entering the docker-build directory'
echo (pwd)
tar -xf io.tar.gz
docker build -t bluechili/meteor-alpine:$argv .
docker push bluechili/meteor-alpine:$argv
echo "\r\r\r"
echo "Build and image version $argv push completed"
rm -rf bundle io.tar.gz
echo "\r\r\r"
echo "Cleanup completed"
