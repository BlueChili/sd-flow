import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Meteor } from 'meteor/meteor';

import './main.html';
import '../imports/ui/dashboard.js';
import '../imports/ui/addOrder.js';
import '../imports/ui/editClient.js';
import '../imports/ui/editOrder.js';
import '../imports/ui/users.js';
import '../imports/ui/takeTask.js';
import '../imports/ui/tasksReport.js';
import '../imports/ui/clients.js';
import '../imports/ui/orders.js';
import '../imports/ui/viewOrder.js';
import '../imports/ui/profile.js';
import '../imports/config/at_config.js';

Template.mainLayout.helpers({
  loggedUser() {
    return !Meteor.user();
  },
});

Accounts.onLogin(function(){
    FlowRouter.go('dashboard');
});

Accounts.onLogout(function(){
  FlowRouter.go('root');
});

FlowRouter.triggers.enter([function(context, redirect){
  if (!Meteor.userId()) {
    FlowRouter.go('root');
  }
}]);

FlowRouter.route('/', {
  name: 'root',
  action: () => {
    if (Meteor.userId()) {
      FlowRouter.go('dashboard');
    }
    BlazeLayout.render('landingPage');
  },
});

FlowRouter.route('/logout', {
  action: () => {
    Meteor.logout();
  },
});
